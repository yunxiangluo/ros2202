# 实验 find_object_2d物体检测

【实验目的】

find_object_2d开源功能包提供了Find-Object应用的ROS封装，能够使用简单的Qt界面选择SIFT、SUFT、FAST等其他特征检测算法，用于物体检测测试。find_object_2d功能包里提供了两个节点：find_object_2d及find_object_3d。其中find_object_3d是为Realsense之类的深度相机准备的，可以通过在匹配目标后识别目标中心的深度信息输出目标的三维坐标。

本实验将使用find_object_2d及find_object_3d节点进行物体检测。

【实验内容】

1. 使用find_object_2d和RealsenseD415相机物体检测；

2. 使用find_object_3d和RealsenseD415相机物体检测；

【软硬件要求】

本次实验的软硬件要求如下：

- 硬件：主机电脑，内存大于8GB；RealsenseD415深度相机

- 软件：Ubuntu16.04操作系统，已安装了ROS Kinetic，并配置了ROS环境。

【实验步骤】

**1. find_object_2d 物体检测**

1)  安装

- 可直接使用apt安装的方式安装find-object-2d功能包：

```
$ sudo apt-get install ros-kinetic-find-object-2d
```

2)  测试find-object-2d节点

- 连接RealsenseD415深度相机，打开终端，输入以下命令驱动相机：

```
$ roslaunch realsense2_camera rs_rgbd.launch
```

- 相机的彩色图像话题名为/camera/color/image_raw，所以在启动find_object_2d节点时，节点的参数image需设为/camera/color/image_raw。新开终端，输入以下命令启动2D检测节点：

```
$ rosrun find_object_2d find_object_2d image:=/camera/color/image_raw 
```

- 启动后，会弹出用于物体检测的GUI窗口Find-Object如下图所示:

![IMG_256](src/images/fig1.png)

- 鼠标右键点击上图Find-Object窗口左侧Objects下的空白页面，会弹出两个选择：Add Object From Scene （从图像添加物体）和Add Object From files（从文件添加物体）。这里我们选择Add Object From Scene 从当前画面中添加需要被检测的物体：

![IMG_256](src/images/fig2.png)

- 在上图点击Take picture 按钮截图拍照

![IMG_256](src/images/fig3.png)

- 在截好的画面上用鼠标左键选择想要识别的物体的区域如下图所示。黄色点表示的是特征点，在使用find_object_2d进行物体检测时，尽量选择特征点较多的物品用于识别。

![IMG_256](src/images/fig4.png)

- 选择好区域后，点击Next按钮得到下图：

![IMG_256](src/images/fig5.png)

若此时的选择符合自己的需求，可点击End按钮结束目标物体的选择。选择的物体会添加到Find-Object窗口左侧的Objects下，此时右侧摄像头拍摄的实时画面中，可以看到被四边形框选出检测到的物体：

- 可按照上面的方式继续在窗口左侧的Objects下右键添加其他物体。

- 新开一个终端，可以使用rostopic echo 命令查看节点find_object_2d节点发布的/objects话题信息：

```
$ rostopic echo /objects
```

![IMG_256](src/images/fig6.png)

- /objects话题的消息类型为std_msgs/Float32MultiArray，data数据的含义：[物体1的ID，物体1的横向宽度，物体1的纵向长度，h11， h12，h13，h21，h22， h23，h31，h32，h33，物体2的ID... ]。hxx是3*3H矩阵。

- 在运行find_object_2d节点的终端中按Ctrl+C关闭程序时，会弹出如下提示框，提示是否想要保存刚才添加的目标物体的图片：

![IMG_256](src/images/fig7.png)

- 若选择Yes，会弹出保存路径选择窗口，选择想要保存的路径，则将以.png图片的格式将目标物体保存下来。

- 再启动find_object_2d节点进行物体检测时，就可以在Objects下鼠标右键选择Add Object From files从上述保存的路径中选择图片添加，同样可以用于物体检测。

- 全部测试完成后关闭所有程序。

**2. find_object_3d 物体检测**

- 新开终端，输入以下命令启动RealsenseD415深度相机：

```
$ roslaunch realsense2_camera rs_rgbd.launch
```

- 输入以下命令启动3D检测节点：

```
$ roslaunch xarm_vision find_object_3d.launch 
```

- 启动后，会弹出用于物体检测的GUI窗口Find-Object，添加检测物体进行检测的操作与2D检测时一致，这里不再赘述：

![IMG_256](src/images/fig8.png)

- 新开一个终端输入以下命令启动RViz，并点击Add按钮添加TF插件，可看到目标物体到camera_link再到base_link的坐标转换关系：

```
$ rosrun rviz rviz
```

![IMG_256](src/images/fig9.png)

 

