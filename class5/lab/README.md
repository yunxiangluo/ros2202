# 实验 XBot-Arm在关节空间内的运动

【实验目的】
用户除了可以通过GUI(RViz中的MotionPLanning插件)和命令行（moveit_commander_cmdline.py）的方式与MoveIt!进行交互外，还可以调用MoveIt!提供的C++或Python接口，通过编程的方式进行轨迹规划和运动控制。本实验将练习Python编程实现XBot-Arm在关节空间内的规划运动。

【实验内容】
1. 教学代码moveit_fk_demo.py的内容，在XBot-Arm 上进行测试；
2. 教学代码moveit_ik_demo.py的内容，在XBot-Arm 上进行测试；
3. 编写Python程序，能够调用不同的API设置目标，实现轨迹规划并执行轨迹，在演示模式和真机上分别对程序进行测试。

【软硬件要求】

本次实验的软硬件要求如下：

硬件：主机电脑，内存大于8GB；

桌面机械臂XBot-Arm；

软件：Ubuntu16.04操作系统，已安装了ROS Kinetic，并配置了ROS环境。

【实验步骤】

**1. 在XBot-Arm 上测试moveit_fk_demo.py程序**

moveit_fk_demo.py是课上教学时使用的代码：调用set_joint_value_target()函数设置一组关节位置作为目标，在关节空间内进行正运动学解算并进行轨迹规划。代码详细说明可参考教学内容，按照下面步骤在XBot-Arm上对程序进行测试。

按“Ctrl+Alt+T”打开新的命令行终端，在终端内输入以下命令启动机械臂的驱动程序（机械臂主机启动）：

```
$ roslaunch xarm_driver xarm_driver.launch
```

- 新开终端，在终端中输入以下命令启动机械臂的MoveIt!相关解算程序（主机从机均可）：

```
$ roslaunch xarm_moveit_config xarm_moveit_planning_execution.launch 
```

- 新开终端，在终端中输入以下命令启动RViz（主机从机均可）：

```
$ roslaunch xarm_moveit_config moveit_rviz.launch config:=true
```

启动后，可以在显示器上看到Rviz界面显示的机械臂状态与实际机械臂的状态一致。

在RViz左侧栏MotionPlanning—Planning Request中取消勾选Query Start State 和Query Goal State：

![1](src/images/fig1.png)

新开终端，输入以下命令启动moveit_fk_demo.py程序（主机从机运行均可）：

```
$ rosrun xarm_moveit_demo moveit_fk_demo.py 
```

程序运行后，可以看到机械臂的动作：运动到初始位置——规划并运动到目标位置——张开手爪——闭合手爪——回到初始位置。

**2. 在XBot-Arm上测试moveit_ik_demo.py程序**

moveit_ik_demo.py是4-1课上教学时使用的代码：调用set_pose_target()函数设置目标位置和姿态，在关节空间内进行逆运动学解算并进行轨迹规划。代码详细说明可参考4-1教学内容，按照下面步骤在XBot-Arm上对程序进行测试。

**注意**：不用关闭第一部分启动的xarm_driver.launch、xarm_moveit_planning_execution.launch和Rviz。若已经关闭，可按照前面的步骤重新启动。

- 新开终端，输入以下命令启动moveit_ik_demo.py程序（主机从机运行均可）：

```
$ rosrun xarm_moveit_demo moveit_ik_demo.py 
```

程序运行后，可以看到机械臂的动作：运动到初始位置——规划并运动到目标位置——末端位置向前移动5厘米——回到初始位置。

测试结束后，可“Ctrl+C”关闭上面终端启动的所有程序。

**3. 编写Python程序实现关节空间内的规划运动**

- 根据代码注释，将xarm_code/xarm_moveit_demo/scripts文件夹内的test_01_fk_ik.py程序补充完整。需要补充TODO注释提示的语句。

- 代码主要内容包含三部分：1）设置目标target_pose，进行轨迹规划并让机械臂运动到target_pose；2)设置目标target_joint_positions，进行轨迹规划并让机械臂运动到target_joint_positions；3）设置目标为初始位置“Home”,进行轨迹规划并让机械臂回到初始位置，程序运行结束。

- 本实验指导书所在目录下提供了供教师参考的test_01_fk_ik.py程序完整版，实验时只提供给学生xarm_code/xarm_moveit_demo/scripts文件夹内非完整版，让学生自己编写代码补充完整。

**代码编写完成后，可先在演示模式下测试编写的代码是否正确：**

- “Ctrl+Alt +T”打开终端，输入以下命令启动demo.launch，出现RViz界面：

```
$ roslaunch xarm_moveit_config demo.launch 
```

- 在RViz左侧栏MotionPlanning—Planning Request中取消勾选Query Start State 和Query Goal State：

![1](src/images/fig2.png)

- 新开终端，输入以下命令启动test_01_fk_ik.py程序：

```
$ rosrun xarm_moveit_demo test_01_fk_ik.py
```

**4.**  **在XBot-Arm 上测试test_01_fk_ik.py程序**

关闭其他程序。 

- 按“Ctrl+Alt+T”打开新的命令行终端，在终端内输入以下命令启动机械臂的驱动程序（机械臂主机启动）：

```
$ roslaunch xarm_driver xarm_driver.launch
```

- 新开终端，在终端中输入以下命令启动机械臂的Moveit!相关解算程序（主机从机启动均可）：

```
$ roslaunch xarm_moveit_config xarm_moveit_planning_execution.launch    
```

- 新开终端，在终端中输入以下命令启动RViz（主机从机均可）：

```
$ roslaunch xarm_moveit_config moveit_rviz.launch config:=true
```

\- 启动后，可以在显示器上看到Rviz界面显示的机械臂状态与实际机械臂的状态一致。

\- 在RViz左侧栏MotionPlanning—Planning Request中取消勾选Query Start State 和Query Goal State：

![1](src/images/fig3.png)

- 新开终端，输入以下命令启动test_01_fk_ik.py程序（主机从机运行均可）：

```
$ rosrun xarm_moveit_demo test_01_fk_ik.py 
```

- 程序运行后，可以看到机械臂的动作：规划并运动到target_pose处——规划并运动到target_joint_positions处——回到初始位置。

