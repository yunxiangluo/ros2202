# 第五讲 关节空间内的规划运动

## 1. MoveIt!的编程接口

本节学习通过编程调用MoveIt！接口的方式控制机械臂在空间内运动。

![123](src/images/fig1.png)

图 1 move_group架构图

MoveIt!的主要API称为Move Group Interface，可以使用C++或Python调用[1] 。这些API提供了用户所需的大部分功能：设置目标位姿、进行运动规划、执行规划轨迹、在规划场景中添加物体、将物体与机械臂进行连接和分离等等。关于API的详细介绍可参考MoveIt!的官方文档：

https://moveit.ros.org/documentation/source-code-api/

C++接口和Python接口的调用大同小异，本课程教学和实验代码样例使用Python API，C++ 版本的编程感兴趣的同学可参考MoveIt!的官方教程[2]。

使用MoveIt! API编程时通常遵循以下几个步骤[1] ：

n 连接到想要控制的规划组，例如桌面机械臂的手臂部分规划组xarm

n 设置目标位姿（关节空间、笛卡尔空间）

n 设置运动约束（可选）

n 使用MoveIt!规划一条可到达目标的轨迹

n 修改轨迹，如更改速度（可选）

n 执行规划出的轨迹

我们将在两种空间内考虑机械臂的运动：关节空间和笛卡尔空间。

当机械臂在关节空间内进行运动时，我们只考虑机械臂点到点运动过程中的避障，不考虑机械臂各个关节以及末端在空间内的运动轨迹。

与之对应的笛卡尔空间内的运动，则需要考虑机械臂末端的轨迹以及各个关节的联动。例如，如果想要控制机械臂末端的笔在纸上写字，在进行规划时，不仅需要让末端从一个点到另一个点，还需要保证末端始终在一个平面上。

下面开始学习如何通过编程实现机械臂在关节空间内的运动规划和轨迹执行。

## 2. 关节空间内的规划运动

### 2.1 正运动学规划示例

示例代码moveit_fk_demo.py在xarm_code/xarm_moveit_demo中。

可先在演示模式下启动查看程序运行效果。

“Ctrl +Alt+ T”打开终端，输入以下命令启动demo.launch，出现RViz界面：

```
$ roslaunch xarm_moveit_config demo.launch 
```

在RViz左侧栏MotionPlanning—Planning Request中取消勾选Query Start State 和Query Goal State：

![1](src/images/fig2.png)

图 2 Planning Request界面

新开终端，输入以下命令启动moveit_fk_demo.py程序：

```
$ rosrun xarm_moveit_demo moveit_fk_demo.py 
```

程序运行后，可以在RViz界面看到机械臂的动作：从初始位置——规划并运动到目标位置——张开手爪——闭合手爪——回到初始位置。虚影是运动规划出的轨迹，实体机械臂的运动表示机械臂正在执行规划出的轨迹。

![1](src/images/fig3.png)

图 3 moveit_fk_demo.py程序运行过程截图

下面对moveit_fk_demo.py程序的详细代码进行解读。完整的API描述可参考官网说明：

http://docs.ros.org/en/noetic/api/moveit_commander/html/classmoveit__commander_1_1move__group_1_1MoveGroupCommander.html#ae7961dd48fff41e3c90f49a722ee6ba3

> import moveit_commander 

导入moveit_commander接口的Python封装，通过它可以获得用来控制机械臂和手爪的所有函数和对象。

> moveit_commander.roscpp_initialize(sys.argv) 

初始化Python API 依赖的moveit_commander C++系统。这一条语句需要写在程序的前面。

> rospy.init_node('moveit_fk_demo', anonymous=True) 

初始化ROS节点，节点名字为moveit_fk_demo。

> arm = moveit_commander.MoveGroupCommander('xarm') 

第三章中，使用MoveIt!的Setup Assistant 配置助手配置桌面机械臂时，为机械臂设置了两个规划组：xarm机械臂组和gripper手爪组，相关配置保存在了SRDF文件中。这条语句通过为MoveGroupCommander类传入规划组名，将arm链接到了xarm规划组。

> gripper = moveit_commander.MoveGroupCommander('gripper') 

同上，将gripper链接到gripper规划组。

> joint_positions = [0, 0, 0, 0, 0, 0] 
> arm.set_joint_value_target(joint_positions) 

xarm规划组包含六个关节：arm_1_joint、arm_2_joint、arm_3_joint、arm_4_joint、arm_5_joint、arm_6_joint。初始位置六个关节的位置都为0 ，单位弧度。使用xarm组六个关节的位置数据进行描述，每个位置对应着一个关节，顺序与SRDF文件中关节顺序对应（详见xarm_moveit_config/config/xarm.srdf）。把一组关节位置通过set_joint_value_target()设定成目标位置。

> traj = arm.plan() 

设置好目标后，通过plan()规划出一条从当前位置到目标的轨迹。规划成功后，函数返回一个RobotTrajectory()对象，该对象包含一条轨迹。将规划出的轨迹保存到traj中。

> arm.execute(traj) 

使用execute()函数来命令机械臂执行轨迹traj。在真实机械臂上，该函数执行后，move_group节点会通过action与机械臂驱动节点进行通信，move_group作为客户端，将包含轨迹在内的目标发送给机械臂驱动节点，机械臂驱动节点再与机械臂下位机通信，控制舵机运动，让机械臂执行轨迹。

如果机械臂一开始便位于初始位置，上述代码很快便可执行完成。

> joint_positions = [-0.664, -0.775, 0.675, -1.241, -0.473, -1.281] 
> arm.set_joint_value_target(joint_positions) 

设置机械臂的一组关节位置作为目标。

> arm.go() 

使用go()进行规划轨迹并执行。go()没有使用中间的轨迹对象，让代码更简洁。在不需要对plan出的轨迹进行二次修改时，可使用go直接进行plan+execute。

> joint_positions = [0.65, 0.65] 
> gripper.set_joint_value_target(joint_positions) 

与xarm规划组的操作类似，这里同样使用set_joint_value_target()函数设置了gripper规划组的一个目标位置，gripper规划组只包含两个转动关节，所有代表目标位置的数组（列表）里只有两个元素，单位弧度。

> gripper.go() 

使用go()进行规划轨迹并执行。运行后，手爪会张开。

> joint_positions = [0, 0] 
> gripper.set_joint_value_target(joint_positions) 
> gripper.go() 

使手爪回到初始位置（闭合状态）。

> arm.set_named_target('Home') 
> arm.go() 

这里我们学习一种新的设置目标的方法。在第三章使用Setup Assistant配置助手时，为桌面机械臂预先定义了几个位姿，其中包括代表机械臂初始位置（所有关节角度为0rad）的“Home”位置。可以通过set_named_target()函数设置xarm规划组的目标为“Home”，然后通过go()使机械臂回到初始位置。

> moveit_commander.roscpp_shutdown() 
> moveit_commander.os._exit(0) 

关闭moveit_commander并退出Python脚本。

可以按照下面步骤在真实机械臂上对程序进行测试。

按“Ctrl+Alt+T”打开新的命令行终端，在终端内输入以下命令启动机械臂的驱动程序（机械臂主机启动）：

```
$ roslaunch xarm_driver xarm_driver.launch
```

新开终端，在终端中输入以下命令启动机械臂的Moveit!相关解算程序（主机从机均可）：

```
$ roslaunch xarm_moveit_config xarm_moveit_planning_execution.launch 
```

\- 新开终端，在终端中输入以下命令启动配置好的RViz（主机从机均可）：

```
$ roslaunch xarm_moveit_config moveit_rviz.launch config:=true
```

启动后，可以在显示器上看到Rviz界面显示的机械臂状态与实际机械臂的状态一致。

新开终端，输入以下命令启动moveit_fk_demo.py程序（主机从机运行都可）：

```
$ rosrun xarm_moveit_demo moveit_fk_demo.py 
```

程序运行后，可以看到机械臂的动作：从初始位置——规划并运动到目标位置——张开手爪——闭合手爪——回到初始位置。与演示模式下的运动基本一致。

### 2.2 逆向运行学规划示例

示例代码moveit_ik_demo.py在xarm_code/xarm_moveit_demo中。

可先在演示模式下启动查看程序运行效果。

“Ctrl+Alt +T”打开终端，输入以下命令启动demo.launch，出现RViz界面：

```
$ roslaunch xarm_moveit_config demo.launch 
```

在RViz左侧栏MotionPlanning—Planning Request中取消勾选Query Start State 和Query Goal State：

![1](src/images/fig4.png)

图 4 Planning Request界面

```
$ rosrun xarm_moveit_demo moveit_ik_demo.py 
```

程序运行后，可以在RViz界面看到机械臂的动作：从初始位置——规划并运动到目标位置——向前移动5厘米—回到初始位置。虚影是运动规划出的轨迹，实体机械臂的运动表示机械臂正在执行规划出的轨迹。

![1](src/images/fig5.png)

图 5 moveit_ik_demo.py程序运行过程截图

下面对moveit_ik_demo.py程序的详细代码进行解读。moveit_ik_demo.py程序中与moveit_fk_demo.py相同的代码将不再进行详细阐述。

> import rospy, sys 
> from math import pi 
> import moveit_commander 
> from geometry_msgs.msg import PoseStamped, Pose 
> import tf 
> from tf.transformations import* 

导入需要的模块库

> moveit_commander.roscpp_initialize(sys.argv) 

初始化Python API 依赖的moveit_commander C++系统。

> rospy.init_node('moveit_ik_demo') 

初始化ROS节点，节点名字为moveit_ik_demo。

> arm = moveit_commander.MoveGroupCommander('xarm') 

通过为MoveGroupCommander类传入规划组名，将arm链接到了xarm规划组。

> end_effector_link = arm.get_end_effector_link() 
> rospy.loginfo("end_effector_link is :"+end_effector_link) 

使用get_end_effector_link()函数获取末端执行器的link。在第二章创建桌面机械臂XBot-Arm的URDF模型时，我们在手爪中心设置了一个虚拟坐标系gripper_centor_link用来表示末端执行器的中心，第三章设置xarm规划组时，将gripper_centor_link也添加到了规划组里用于规划，所以这里获取到的末端执行器link是gripper_centor_link。

> reference_frame = 'base_link' 
> arm.set_pose_reference_frame(reference_frame) 

显示地设置目标位置的参考系为base_link。

> arm.allow_replanning(True) 

若allow_replanning()参数为True，则MoveIt!在一次规划失败后进行重新规划；若参数为False，则只会尝试一种。通常设置为True。

> arm.set_goal_position_tolerance(0.02) 
> arm.set_goal_orientation_tolerance(0.03) 

设置进行逆运动学解算时能接受的位置和姿态的容忍度（误差）。第一句设置了位置容忍度为0.02米，第二句设置了姿态容忍度为0.03弧度。我们可以根据实际情况设置合适的容忍度。

> arm.set_max_acceleration_scaling_factor(0.6) 
> arm.set_max_velocity_scaling_factor(0.35) 

设置允许的最大加速度和最大速度。

> arm.set_named_target('Home') 
> arm.go() 
> rospy.sleep(1) 

机械臂回到初始位置“Home”。

> target_pose = PoseStamped() 
> target_pose.header.frame_id = reference_frame 
> target_pose.header.stamp = rospy.Time.now() 

设置末端执行执行器的目标位姿。参考系为之前设置的reference_frame，也就是base_link。

> target_pose.pose.position.x = 0.3 
> target_pose.pose.position.y = 0.1 
> target_pose.pose.position.z = 0.25 

设置目标的位置xyz，末端执行器（gripper_base_link）是在base_link坐标系下，x为0.3米，y为0.1米，高为0.25米的位置。

> quaternion = tf.transformations.quaternion_from_euler(0,pi/2,0) 
> target_pose.pose.orientation.x = quaternion[0] 
> target_pose.pose.orientation.y = quaternion[1] 
> target_pose.pose.orientation.z = quaternion[2] 
> target_pose.pose.orientation.w = quaternion[3] 

设置目标的姿态。第一句用quaternion_from_euler()函数将欧拉角表示的旋转转化为四元数quaternion表示，三个参数分别代表Roll、Pitch、Yaw。Pitch为π/2，代表gripper_centor_link相对于base_link绕X轴旋转90度，也就是手爪竖直向下的状态。

> arm.set_start_state_to_current_state() 

把目标位姿发给逆运动学（IK）求解器之前，显示地把开始状态设置为机械臂的当前状态。

> arm.set_pose_target(target_pose, end_effector_link) 

使用set_pose_target函数为末端执行器设置目标位姿，函数的第一个参数为目标位姿，第二个参数为末端link的名字，这里为gripper_centor_link。这个函数与set_joint_value_target()函数类似。

> traj = arm.plan() 
> arm.execute(traj) 

plan()规划轨迹，execute()执行轨迹。

> current_pose = arm.get_current_pose(end_effector_link) 
> current_joint_positions = arm.get_current_joint_values() 

第一句获取末端执行器当前的位姿，第二句获取机械臂六个关节当前的位置。

> arm.shift_pose_target(0,0.05,end_effector_link) 
> arm.go() 

可以通过shift_pose_target()函数只在某个方向改变机械臂末端的位置和姿态。函数第一个参数决定了在哪个轴上平移或旋转，按照0、1、2、3、4、5的顺序依次定义x、y、z、r、p、y轴，其中r、p、y分别代表横摇、纵摇和纵倾。上面代码设定了一个沿着X轴正方向平移5厘米的操作。

> arm.set_named_target('Home') 
> arm.go() 

机械臂回到初始状态。

> moveit_commander.roscpp_shutdown() 
> moveit_commander.os._exit(0) 

关闭moveit_commander并退出程序。

可以按照下面步骤在真实机械臂上对程序进行测试。

按“Ctrl+Alt+T”打开新的命令行终端，在终端内输入以下命令启动机械臂的驱动程序（机械臂主机启动）：

```
$ roslaunch xarm_driver xarm_driver.launch
```

\- 新开终端，在终端中输入以下命令启动机械臂的Moveit!相关解算程序（主机从机均可）：

```
$ roslaunch xarm_moveit_config xarm_moveit_planning_execution.launch  
```

\- 新开终端，在终端中输入以下命令启动配置好的RViz（主机从机均可）：

```
$ roslaunch xarm_moveit_config moveit_rviz.launch config:=true
```

\- 启动后，可以在显示器上看到Rviz界面显示的机械臂状态与实际机械臂的状态一致。

\- 新开终端，输入以下命令启动moveit_ik_demo.py程序（主机从机运行都可）：

```
$ rosrun xarm_moveit_demo moveit_ik_demo.py 
```

程序运行后，可以看到机械臂的动作：从初始位置——规划并运动到目标位置——末端位置向前移动5厘米——回到初始位置。与演示模式下的运动基本一致。

## 3. 小结

编程实现机械臂在关节空间内运动的关键步骤

n 创建规划组的控制对象： 

arm = moveit_commander.MoveGroupCommander('xarm')

  设置目标：set_joint_value_target()、set_named_target()、set_pose_target()、shift_pose_target()等

n 设置运动约束（可选）：set_max_velocity_scaling_factor()、set_max_acceleration_scaling_factor()等

n 使用MoveIt!规划一条可到达目标的轨迹：traj = arm.plan()

n 执行规划出的轨迹：arm.execute(traj) 、arm.go()



 